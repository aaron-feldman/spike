# Bootstrap for PreNav test servers to have couchbase and other basic tools.

echo "Running Bootstrap"
echo "Installing Git"
sudo apt-get install git 
echo "Installing libcouchbase"
sudo wget -O/etc/apt/sources.list.d/couchbase.list http://packages.couchbase.com/ubuntu/couchbase-ubuntu1204.list
wget -O- http://packages.couchbase.com/ubuntu/couchbase.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install libcouchbase2-libevent libcouchbase-dev
echo "Installing pip"
sudo apt-get install python-pip
echo "Installing pip3"
sudo apt-get install python3-pip
echo "Installing python couchbase"
sudo pip3 install couchbase
echo "Installing boto3"
sudo pip3 install boto3



